import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GerenciarLogComponent } from './gerenciar-log.component';

describe('GerenciarLogComponent', () => {
  let component: GerenciarLogComponent;
  let fixture: ComponentFixture<GerenciarLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GerenciarLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GerenciarLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
