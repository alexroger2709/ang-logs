import { Injectable } from '@angular/core';
import { DataService } from './data.service';

import { CriarLogRequest } from '../criar-log-request';
import { ListarLogRequest } from '../listar-log-request';
import { AtualizarLogRequest } from '../atualizar-log-request';
import { ExcluirLogRequest } from '../excluir-log-request';


import { Http } from '@angular/http';

@Injectable()
export class LogDataService extends DataService{

    constructor(http: Http) { 
      super('http://localhost:8080/logsApp/',http);
    }



    criar(requestObj: CriarLogRequest){
      return super.criar(requestObj);
    }


    listar(requestObj: ListarLogRequest){
      return super.listar(requestObj);
    }


    atualizar(requestObj: AtualizarLogRequest){
      return super.atualizar(requestObj);
    }


    excluir(requestObj: ExcluirLogRequest){
      return super.excluir(requestObj);
    }
  
}
