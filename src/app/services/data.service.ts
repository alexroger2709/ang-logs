import { Injectable } from '@angular/core';

import { Headers, Response } from '@angular/http';
import { HttpClient, HttpParams } from '@angular/common/http';
import { RequestOptions, Request, RequestMethod } from '@angular/http';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';




@Injectable()
export class DataService {

  //send universal parameters
  private headers = new Headers({
    'Access-Control-Allow-Origin':'*',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
    'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE'
  });
  private options = new RequestOptions({ headers: this.headers });


  constructor(private url: string, private http: Http) { }

  criarLote(fileToUpload: File): Observable<boolean> {

      const uploadHeaders = new Headers({
        'Access-Control-Allow-Origin':'*',
        'Accept': 'multipart/form-data',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization',
        'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
        'Access-Control-Allow-Credentials': 'true'
      });
  
      const endpoint = this.url + 'criarLogLote';
      const formData: FormData = new FormData();
      formData.append('file', fileToUpload, fileToUpload.name);
      return this.http
        .post(endpoint, formData, { headers: uploadHeaders })
        .map(() => { return true; })
        .catch((e: any) => Observable.throw(this.tratarErroUpload(e)));
}

  tratarErroUpload(error: any){
    console.log(error);
  }


  criar(objParams: any){
    let url = this.url + 'criarLog';  
    let dataParams = JSON.stringify(objParams);

    return this.http.post(url, dataParams, this.options)
      .map(response=>response.json())
    ;
  }


  listar(objParams: any){
    let url = this.url + 'listarLog';  
    let dataParams = JSON.stringify(objParams);
    return this.http.post(url, dataParams, this.options)
      .map(response=>response.json())
    ;
  }


  atualizar(objParams: any){
    let url = this.url + 'atualizarLog';  
    let dataParams = JSON.stringify(objParams);

    return this.http.put(url, dataParams, this.options)
      .map(response=>response.json())
    ;
  }


  excluir(objParams: any){
    let url = this.url + 'excluirLog';  
    let dataParams = JSON.stringify(objParams);

    return this.http.delete(url, dataParams)
      .map(response=>response.json())
    ;
  }




}
