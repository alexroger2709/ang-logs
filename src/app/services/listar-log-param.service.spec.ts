import { TestBed, inject } from '@angular/core/testing';

import { ListarLogParamService } from './listar-log-param.service';

describe('ListarLogParamService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListarLogParamService]
    });
  });

  it('should be created', inject([ListarLogParamService], (service: ListarLogParamService) => {
    expect(service).toBeTruthy();
  }));
});
