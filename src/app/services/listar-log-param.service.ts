import { Injectable } from '@angular/core';
import { ListarLogRequest } from '../listar-log-request';


@Injectable()
export class ListarLogParamService {
  
  logRequest: ListarLogRequest;

  constructor() { }

  //utilizamos este serviço para preservar os paramêtros das listagens
  setParams(logRequest: ListarLogRequest){
    this.logRequest = logRequest;
  }

  getParams(){
    if(this.logRequest == undefined){
      this.logRequest = new ListarLogRequest();
      this.logRequest.setPaginaAtual(1);
      this.logRequest.setRegistrosPorPagina(10);
      this.logRequest.setId(0);
      this.logRequest.setDataInicio('');
      this.logRequest.setDataFim('');
      this.logRequest.setIp('');
      this.logRequest.setRequest('');
      this.logRequest.setStatus(0);
      this.logRequest.setUserAgent('');
    }

    return this.logRequest;
  }

}
