export class ListarLogRequest{

    constructor(private paginaAtual?:number,
                private registrosPorPagina?:number,
                private id?:number,
                private dataInicio?:string,
                private dataFim?:string,
                private ip?:string,
                private request?:string,
                private status?:number,
                private userAgent?:string){}


    //propriedades
    setPaginaAtual(paginaAtual:number){
        this.paginaAtual = paginaAtual;
    }

    getPaginaAtual(){
        return this.paginaAtual;
    }


    
    setRegistrosPorPagina(registrosPorPagina:number){
        this.registrosPorPagina = registrosPorPagina;
    }

    getRegistrosPorPagina(){
        return this.registrosPorPagina;
    }



    setId(id:number){
        this.id = id;
    }

    getId(){
        return this.id;
    }




    setDataInicio(dataInicio:string){
        this.dataInicio = dataInicio;
    }

    getDataInicio(){
        return this.dataInicio;
    }


    setDataFim(dataFim:string){
        this.dataFim = dataFim;
    }

    getDataFim(){
        return this.dataFim;
    }


    setIp(ip:string){
        this.ip = ip;
    }

    getIp(){
        return this.ip;
    }



    setRequest(request:string){
        this.request = request;
    }

    getRequest(){
        return this.request;
    }


    setStatus(status:number){
        this.status = status;
    }

    getStatus(){
        return this.status;
    }



    setUserAgent(userAgent:string){
        this.userAgent = userAgent;
    }

    getUserAgent(){
        return this.userAgent;
    }



}