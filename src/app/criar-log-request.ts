export class CriarLogRequest{

    constructor(private data?:string,
                private ip?:string,
                private request?:string,
                private status?:number,
                private userAgent?:string){}


    //propriedades


    setData(data:string){
        this.data = data;
    }

    getData(){
        return this.data;
    }


    setIp(ip:string){
        this.ip = ip;
    }

    getIp(){
        return this.ip;
    }



    setRequest(request:string){
        this.request = request;
    }

    getRequest(){
        return this.request;
    }


    setStatus(status:number){
        this.status = status;
    }

    getStatus(){
        return this.status;
    }



    setUserAgent(userAgent:string){
        this.userAgent = userAgent;
    }

    getUserAgent(){
        return this.userAgent;
    }



}