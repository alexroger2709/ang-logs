import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { LogDataService } from './services/log-data.service';
import { ListarLogParamService } from './services/listar-log-param.service';



import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ListarLogComponent } from './listar-log/listar-log.component';
import { LoteLogComponent } from './lote-log/lote-log.component';
import { GerenciarLogComponent } from './gerenciar-log/gerenciar-log.component';


@NgModule({
  declarations: [
    AppComponent,
    ListarLogComponent,
    LoteLogComponent,
    GerenciarLogComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [
    LogDataService,
    ListarLogParamService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
