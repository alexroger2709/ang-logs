import { Component, OnInit } from '@angular/core';
import { LogDataService } from '../services/log-data.service';

@Component({
  selector: 'app-lote-log',
  templateUrl: './lote-log.component.html',
  styleUrls: ['./lote-log.component.css']
})
export class LoteLogComponent implements OnInit {


  fileToUpload: File = null;
  
  constructor(private logDataService: LogDataService) { }

  ngOnInit() {
  }


  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.uploadFileToActivity();
  } 


  uploadFileToActivity() {
    this.logDataService.criarLote(this.fileToUpload).subscribe(data => {
      // sucesso
        console.log('sucesso!');
      }, error => {
        console.log(error);
      });
  }
  
  

}
