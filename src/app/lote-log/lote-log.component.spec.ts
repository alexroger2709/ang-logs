import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoteLogComponent } from './lote-log.component';

describe('LoteLogComponent', () => {
  let component: LoteLogComponent;
  let fixture: ComponentFixture<LoteLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoteLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoteLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
