import { Component, OnInit } from '@angular/core';
import { LogDataService } from '../services/log-data.service';
import { ListarLogParamService } from '../services/listar-log-param.service';
import { ListarLogRequest } from '../listar-log-request';

@Component({
  selector: 'app-listar-log',
  templateUrl: './listar-log.component.html',
  styleUrls: ['./listar-log.component.css']
})
export class ListarLogComponent implements OnInit {

  logResponse: any[];
  logRequest: ListarLogRequest;
  operationCode: number;
  operationMsg: string;
  currentPage: number;
  totalPages: number;
  totalRec: number;

  page: number;
  recsPerPage: number;



  constructor(
    private logDataService: LogDataService,
    private logParamService: ListarLogParamService
  ) { }

  ngOnInit() {
    //obtém os paramêtros salvos pelo serviço
    this.logRequest = this.logParamService.getParams();

    //tenta listar as informações
    this.search();

  }


  search(){
    this.page = this.logRequest.getPaginaAtual();
    this.recsPerPage = this.logRequest.getRegistrosPorPagina();

    this.logDataService.listar(this.logRequest)
    .subscribe(logResponse =>{
      this.logResponse = logResponse.logs;
      if((logResponse.logs.length)>0){
        this.operationCode = 0;
        this.operationMsg = '';
        this.currentPage = this.logRequest.getPaginaAtual();
        this.totalPages = logResponse.logs[0].totalPaginas;
        this.totalRec = logResponse.logs[0].totalRegistros;
      }
    });  

    this.logParamService.setParams(this.logRequest);
  }


  //verify status pages
  firstPage(){
    if(this.currentPage == 1){
      return true;
    }else{
      return false;
    }
  }

  lastPage(){
    if(this.currentPage == this.totalPages){
      return true;
    }else{
      return false;
    }
  }


  goFirstPage(){
    this.logRequest.setPaginaAtual(1);
    this.search();
  }

  goLastPage(){
    this.logRequest.setPaginaAtual(this.totalPages);
    this.search();
  }

  goNextPage(){
    if ( (this.currentPage+1) <= this.totalPages){
      this.logRequest.setPaginaAtual(this.currentPage+1);
      this.search();
    }
  }

  goPriorPage(){
    if ( (this.currentPage-1) >= 1 ){
      this.logRequest.setPaginaAtual(this.currentPage-1);
      this.search();
    }
  }

  hasNext(){
    if(this.currentPage < this.totalPages){
      return true;
    }else{
      return false;
    }
  }

  hasPrior(){
    if(this.currentPage > 1){
      return true;
    }else{
      return false;
    }
  }



  changeRecsPerPage(recValue){
    this.logRequest.setRegistrosPorPagina(recValue);
    this.logRequest.setPaginaAtual(1);
    this.search();
  }

  goToPage(pageNumber){
    if ( (pageNumber > 0 ) && (pageNumber <= this.totalPages)){
      this.logRequest.setPaginaAtual(pageNumber);
      this.search();
    }
  }

}
